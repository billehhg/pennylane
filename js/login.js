/* checks password and username */
function log_in() {
	if ( $("#username").val() == "" ) {
		$("#warning").html("You must enter a username");
	}else{
		var login = $("#username").val();
		var passwd = $("#password").val();
		
		/* this is the format to push variables to php
		 * feel free to copy and paste this wherever 
		 * you just need to change the variables       */
		$.ajax({
			url: "/php/login.php",
			type: "POST",
			data: {
				login: login,
				password: passwd
			},
			/* this part runs if it gets something back from the php file */
			success: function(data, status, xhr) {
				/* php won't always send "error"
				 * you need to tell it to   */
				if (data.indexOf("error") > -1) {
					$("#warning").html("Wrong username or password");
				}else{
					window.location = "/home/index.php";
				}
			}
		});
	}
}

/* adds password confirmation input to create account */
function create_account() {
	if ( $("#username").val() == "" ) {
		$("#warning").html("You must enter a username");
	}
	// we can make usernames longer but why would we want to?
	else if ( $("#username").val().length > 30 ) {
		$("#warning").html("Username is too long, please make one less than 30 characters");
	}else{
		$("#pass_conf").show();
		$("#cancel").show();
		$("#login").hide();
		$("#create").click( function() { create_confirm(); } )
	}
}

/* checks if passwords match, then creates account using php */
function create_confirm() {
	if ( $("#username").val() == "" ) {
		$("#warning").html("You must enter a username");
	}
	else if ( $("#username").val().length > 30 ) {
		$("#warning").html("Username is too long, please make one less than 30 characters");
	}else{
		if ( $("#pass_conf").val() == $("#password").val() ) {
			var login = $("#username").val();
			var passwd = $("#password").val();
			
			$.ajax({
				url: "../php/create.php",
				type: "POST",
				data: {
					login: login,
					password: passwd
				},
				success: function(data, status, xhr) {
					if (data.indexOf("error") > -1) {
						$("#warning").html(data);
					}else{
						$("#warning").html("");
						$("#success").html("Username "+login+" created. Login now");
						$("#login").show();
						$("#pass_conf").hide();
						$("#cancel").hide();
						$("#create").hide();
					}
				}
			});
		}
		else{
			$("#warning").html("Passwords do not match");
		}
	}
}

$( document ).ready(function() {
	$("#username").keyup(function (event) { 
		if (event.keyCode == 13) {
			$("#password").focus();
		}
	});

	$("#password").keyup(function (event) { 
		if (event.keyCode == 13) {
			if ($("#pass_conf").is(":visible")) {
				$("#pass_conf").focus();
			}else{
				$("#login").click();
			}
		}
	});

	$("#pass_conf").keyup(function (event) { 
		if (event.keyCode == 13) {
			$("#create").click();
		}
	});
});
