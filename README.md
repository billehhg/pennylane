# pennylane

pennylane is a set of web tools designed to make managing schedules with friends or family private and simple.

## Getting Started

Hosting this should be fairly simple without too much configuration. Just git clone into your own server environment.

### Prerequisites

For servers Linux or BSD are recommended but any operating system will do.
You will need to install your own web stack. 
* Web Server. We used [lighttpd](https://www.lighttpd.net/). Apache is also popular. Both should be available natively on Linux.
* SQL. We used [MariaDB](https://mariadb.org/). It should also be available natively.
* [PHP](https://secure.php.net/). Again, certainly available natively.
* [uglify](https://github.com/mishoo/UglifyJS) for shrinking js code.
* [jquery](https://github.com/jquery/jquery)

### Installing

Once your web server is set up, just clone this repository into your repo and change connect.php to match the name you chose for your database.

## Contributing

We welcome contributions of any kind. If you try it out, leave bug reports!

To start developing, simply type:

```bash
# git clone https://gitlab.com/billehhg/pennylane.git
```

template.html exists for every php file. These are for editing the html and css without needing to run php.

Some organizational preferences:
* separate topics should go in separate directories (calendar in one, shopping list in another)
* if a topic needs two pages, put them both in the same directory, or in sub directories
* any scripts, styles or php that are used by more than one page go in the root directory (i.e. /css or /js)
* monolithic css is preferable
* js needn't be monolithic but avoid loading more than one or two separate scripts per page

Some coding preferences:
* Obviously make it pretty and organized. Don't shorten javascript too much on your own
* for javascript and css curley braces at end of line

```javascript
function something () {
    alert("Hello world!");
}
```

```css
#example {
    color: blue;
}
```
* for php, curly braces on new line

```php
if ($x == 0)
{
    echo $x;
}
```
* uglify your js files and put them into another directory labeled ugly

## Authors

* **William Kroes** - MariaDB, php, js, css, html

* **Hannah Deahl** - js, css, html
