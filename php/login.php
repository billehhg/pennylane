<?php
include "connect.php";
session_start();

$username = mysqli_real_escape_string($link, $_POST['login']);
$password = mysqli_real_escape_string($link, $_POST['password']);

$sql = "SELECT username, password
    	FROM user
     	WHERE username = '$username'";

$result = mysqli_query($link, $sql);
while ($row = mysqli_fetch_array($result)) 
{
    $username_conf = $row[0];
    $password_hashed = $row[1];
}

if (crypt($password, $password_hashed) == $password_hashed)
{
    echo "success";
    $_SESSION["username"] = $username_conf;
}
else
{
    echo "error";
    session_unset();
    session_destroy();
}

mysqli_close($link);
