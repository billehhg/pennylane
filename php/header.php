<script type="text/javascript" src="../js/meta.js"></script>
<?php
include "connect.php";
session_start();
$username = $_SESSION["username"];
if ($username == "")
{
	echo "<a href='../'>Login Page</a></br>";
	die("You must log in to view this site");
}
$pages = array("home");

$ret_val = "<ul class='bar topbar'>";
for ($i = 0; $i < count($pages); $i++)
{
	$ret_val .= "<li><a href='../".$pages[$i]."' ";
	if ($pages[$i] == $page)
	{
		$ret_val .= "class='active'";
	}
	$ret_val .= ">".$pages[$i]."</a></li>";
}
$ret_val .= "<li class='right'><a id='logout'>logout</a></li><li class='right'><a>$username</a></li></ul>";

echo $ret_val;
