$(document).ready(function() {
	load_calendar();
	$("#left_cal").click(function() {
		load_calendar(-1);
	});
	$("#right_cal").click(function() {
		load_calendar(1);
	});
	$(".time").focus(function() {
		locate = $(this).position();
		$("select").css("left",locate.left);
		$("select").slideDown();
		var time_box = $(this).attr("id");
		$("select").change(function() {
			$("#"+time_box).val($(this).val());
			$(this).hide();
			time_box = null;
		});
		$("input:not(.time)").focus(function(){
			$("select").hide();
		});
	});
	$("#addevent").click(function() {
	});
});

function load_calendar(month_offset=0){
	var months = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
	var cur_date = $("#currentmonth").html().split("<br>");
	var cur_month = months.indexOf(cur_date[0]) + month_offset;
	var cur_year = cur_date[1];
	if ( cur_month == -1 ) {
		cur_year -= 1;
		cur_month = months[11];
	} else if ( cur_month == 12 ) {
		/*
		 * Javascript is pretty damned stupid
		 * string-int=int
		 * string+int=string+int
		 * "2018" + 1 = "20181"
		 * "2018" - -1 = 2019
		 */
		cur_year -= -1;
		cur_month = months[0];
	} else {
		cur_month = months[cur_month];
	}

	current_month = cur_month + "<br>" + cur_year;
	$("#currentmonth").html(current_month);

	$.ajax({
		url: "/home/php/cal_gen.php",
		type: "POST",
		data: {
			month: cur_month + " " + cur_year
		},
		success: function(data, status, xhr) {
			$("#calendar").html(data);
			$("tr.calendar th").click(function() {
				$(".selected").removeClass("selected");
				$(this).addClass("selected");
				show_day();
			});
			$(".today").click();
		}
	});
}

function show_day() {
	var day = $(".selected").html();
	var cur_date = $("#currentmonth").html().split("<br>");
	var month = cur_date[0];
	var year = cur_date[1];
	var date = month + " " + day + " " + year;
	$("#selectedday h2").html(date);
	events = "";
	if (events == "") {
		$("#selectedday ul").html("<li>No events to display</li>");
	}
}

