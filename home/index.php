<?php
session_start();
?>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Pennylane Home</title>
    <link rel="stylesheet" href="../css/styles.css" />
	<script src="/external/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="js/home.js"></script>
</head>
<body>
	<?php 
	$page = "home"; 
	include "../php/header.php"; 
	?>
	<div class="container">
		<div class="leftcontain calendar">
			<div class="title">
				<h2 id="left_cal">&#10094;</h2>
				<h2 id="currentmonth"><?php echo date("M")."<br />".date(" Y"); ?></h2>
				<h2 id="right_cal">&#10095;</h2>
			</div>
			<div id="calendar">
			</div>
		</div>
		<div id="selectedday" class="rightcontain listbox">
			<div class="title"><h2></h2></div>
			<ul>
			</ul>
			<input id='addevent' type='button' value='+ Add Event'>
		</div>
		<div class="rightcontain listbox">
			<div class=title><h2>Upcoming Events</h2></div>
			<div class="container">
				<ul id="upcoming">
					<li>No upcoming events</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="popup">
		<div class="container">
			<input type="text" id="eventtitle" name="eventtitle" placeholder="Event Title">
			<br>
			<input class="time" id="start"><b>-</b><input class="time" id="end">
			<span id="timeSelector">
				<select size="6">
					<option>12:00 AM</option>
					<option>1:00 AM</option>
					<option>2:00 AM</option>
					<option>3:00 AM</option>
					<option>4:00 AM</option>
					<option>5:00 AM</option>
					<option>6:00 AM</option>
					<option>7:00 AM</option>
					<option>8:00 AM</option>
					<option>9:00 AM</option>
					<option>10:00 AM</option>
					<option>11:00 AM</option>
					<option>12:00 PM</option>
					<option>1:00 PM</option>
					<option>2:00 PM</option>
					<option>3:00 PM</option>
					<option>4:00 PM</option>
					<option>5:00 PM</option>
					<option>6:00 PM</option>
					<option>7:00 PM</option>
					<option>8:00 PM</option>
					<option>9:00 PM</option>
					<option>10:00 PM</option>
					<option>11:00 PM</option>
				</select>
			</span>
		</div>
	</div>
</body>
</html>
