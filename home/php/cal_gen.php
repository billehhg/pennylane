<?php
	include "../../php/connect.php";

	$month = mysqli_real_escape_string($link, $_POST['month']);

	$days = array("Sun","Mon","Tue","Wed","Thu","Fri","Sun");
	$ret_str = "<table><tr class='days calendar'>";
	for ($x = 0; $x <= 6; $x++) 
	{
		$ret_str .= "<th ";
		$ret_str .= ">".$days[$x][0]."</th>";
	}
	$ret_str .= "</tr>";
	$start_date = strtotime($month); 

	$start_day = array_search(date('D', $start_date),$days);
	$total_days_prev = date('t', strtotime("-1 month", $start_date));
	$total_days = date('t', strtotime($month));

	// there's a reason for this
	// TODO: When I'm less tired, figure out the reason for this
	if ( $start_day > 0 ) {
		$cal_day = $total_days_prev - $start_day + 1;
		$canbetoday = False;
	}else{
		$cal_day = 1;
		$canbetoday = True;
	}

	$ret_str .= "<tr class='calendar'>";
	for ($x = 0; $x <= 6; $x++) 
	{
		$ret_str .= "<th class='";
		if ($canbetoday)
		{
			$ret_str .= "month";
			if (strtotime($cal_day." ".$month) == strtotime(date("d M Y")))
			 {
				$ret_str .= " today";
			}
		}
		$ret_str .= "'>".$cal_day."</th>";
		if ($cal_day == $total_days_prev)
		{
			$cal_day = 0;
			$canbetoday = True;
		}
		$cal_day++;
	}
	$ret_str .= "</tr>";

	for ($c = 0; $c <=4; $c++)
	{
		$ret_str .= "<tr class='calendar'>";
		for ($x = 0; $x <= 6; $x++) 
		{
			$ret_str .= "<th class='";
			if ($canbetoday)
			{
				$ret_str .= "month";
				if (strtotime($cal_day." ".$month) == strtotime(date("d M Y")))
				 {
					$ret_str .= " today";
				}
			}
			$ret_str .= "'>".$cal_day."</th>";
			if ($cal_day == $total_days)
			{
				$cal_day = 0;
				$canbetoday = False;
			}
			$cal_day++;
		}
		$ret_str .= "</tr>";
	}
	$ret_str .= "</table>";
	
	echo $ret_str;
