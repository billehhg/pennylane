<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="css/styles.css" />
	<script src="external/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src="ugly/login.min.js"></script>
</head>
<body class="login">
	<div id="auth_container" class="login">
		<div id="warning"></div>
		<div id="success"></div>
		<input type="text" id="username" name="username" placeholder="Username" autofocus="autofocus">
		<input type="password" id="password" name="password" placeholder="Password">
		<input type="password" id="pass_conf" name="pass_conf" placeholder="Confirm password" style="display:none;">
		<input type="button" id="login" name="login" value="Login" onclick="log_in()">
		<br>
		<input type="button" id="create" name="create" value="Create Account" onclick="create_account()">
		<a href="." id="cancel">Cancel</a>
	</div>
</body>
</html>
